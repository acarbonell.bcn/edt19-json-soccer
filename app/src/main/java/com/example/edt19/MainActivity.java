package com.example.edt19;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Spinner sp;
    private CountryAdapter sAdapter;
    private ArrayList<Country> list;
    public Button btnNext;

    public static String EXTRA_TEXT_NAME = "com.example.edt19.EXTRA_TEXT_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sp = findViewById(R.id.spinnerCountry);
        btnNext = findViewById(R.id.btnNext);

        initData();

        sAdapter = new CountryAdapter(this, list);
        sp.setAdapter(sAdapter);

        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Country itemSelect = (Country) adapterView.getItemAtPosition(i);
                String countrySel = itemSelect.getTextCountry();
                Toast.makeText(MainActivity.this, countrySel, Toast.LENGTH_SHORT).show();
                btnNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this,  DetailActivity.class);
                        intent.putExtra(EXTRA_TEXT_NAME, countrySel);

                        startActivity(intent);
                    }
                });


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void initData(){
        list = new ArrayList<>();
        list.add(new Country("Brazil", "https://www.countryflags.com/wp-content/uploads/brazil-flag-png-large.png"));
        list.add(new Country("Canada", "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"));
        list.add(new Country("China", "https://www.countryflags.com/wp-content/uploads/china-flag-png-large.png"));
        list.add(new Country("Colombia", "https://www.countryflags.com/wp-content/uploads/colombia-flag-png-large.png"));
        list.add(new Country("Germany", "https://www.countryflags.com/wp-content/uploads/germany-flag-png-large.png"));
        list.add(new Country("Denmark", "https://www.countryflags.com/wp-content/uploads/denmark-flag-png-large.png"));
        list.add(new Country("Estonia", "https://www.countryflags.com/wp-content/uploads/estonia-flag-png-large.png"));
        list.add(new Country("England", "https://www.countryflags.com/wp-content/uploads/united-kingdom-flag-png-large.png"));
        list.add(new Country("Spain", "https://www.countryflags.com/wp-content/uploads/spain-flag-png-large.png"));
        list.add(new Country("Finland", "https://www.countryflags.com/wp-content/uploads/finland-flag-png-large.png"));

    }


}