package com.example.edt19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import java.util.ArrayList;

public class ImagesActivity extends AppCompatActivity {
    //private ViewPager viewPager;
    ArrayList<String> images = new ArrayList<>();
    /*private String[] images = new String[] {
            "https://www.thesportsdb.com/images/media/league/fanart/1q4gqv1544977674.jpg",
            "https://www.thesportsdb.com/images/media/league/fanart/h9ykmq1544977787.jpg",
            "https://www.thesportsdb.com/images/media/league/fanart/4mlh0m1544977860.jpg",
            "https://www.thesportsdb.com/images/media/league/fanart/9pczc21548795769.jpg",
    };*/


   /* private int[] images = new int[]{
            R.drawable.animal01,
            R.drawable.animal02,
            R.drawable.animal03,
            R.drawable.animal04
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        getData();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        ImageAdapter imageAdapter = new ImageAdapter(this, images);
        viewPager.setAdapter(imageAdapter);
    }

    private void getData(){
        if(getIntent().hasExtra("images")){
            images = getIntent().getStringArrayListExtra("images");
        }
    }
}