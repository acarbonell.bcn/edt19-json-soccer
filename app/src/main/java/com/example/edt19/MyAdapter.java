package com.example.edt19;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context context;
    private List<Sport> sports;

    public MyAdapter(Context context, List<Sport> sports) {
        this.context = context;
        this.sports = sports;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.detail_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(sports.get(position).getStrBadge()).fit().centerCrop().into(holder.strBadge);
        holder.strLeague.setText(sports.get(position).getStrLeague());
        holder.strDescriptionEN.setText(sports.get(position).getStrDescriptionEN());
        holder.buttonWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("https://" + sports.get(holder.getAdapterPosition()).getStrWebsite()));
                context.startActivity(intent);
            }
        });
        ArrayList<String> images = new ArrayList<>();
            images.add(sports.get(position).getStrFanart1());
            images.add(sports.get(position).getStrFanart2());
            images.add(sports.get(position).getStrFanart3());
            images.add(sports.get(position).getStrFanart4());

        holder.buttonImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImagesActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putStringArrayListExtra("images", images);
                context.startActivity(intent);

            }
        });



    }

    @Override
    public int getItemCount() {
        return sports.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView strBadge;
        TextView strLeague;
        TextView strDescriptionEN;
        Button buttonWeb;
        Button buttonImg;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            strBadge = itemView.findViewById(R.id.flag);
            strLeague = itemView.findViewById(R.id.textTitle);
            strDescriptionEN = itemView.findViewById(R.id.textDesc);
            buttonWeb = itemView.findViewById(R.id.buttonWeb);
            buttonImg = itemView.findViewById(R.id.buttonImgs);
        }
    }
}
