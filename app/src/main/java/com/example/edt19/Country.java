package com.example.edt19;

public class Country {
    private String textCountry;
    private String imgCountry;

    public Country() {
    }

    public Country(String textCountry, String imgCountry) {
        this.textCountry = textCountry;
        this.imgCountry = imgCountry;
    }

    public String getTextCountry() {
        return textCountry;
    }

    public void setTextCountry(String textCountry) {
        this.textCountry = textCountry;
    }

    public String getImgCountry() {
        return imgCountry;
    }

    public void setImgCountry(String imgCountry) {
        this.imgCountry = imgCountry;
    }
}
