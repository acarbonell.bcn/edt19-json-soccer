package com.example.edt19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DetailActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    List<Sport> sports = new ArrayList<>();
    MyAdapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        recyclerView = findViewById(R.id.rViewDetail);


        getSports();
    }

    private void getSports(){
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                createUrl(),
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("countrys");
                            for (int i = 0; i < response.getJSONArray("countrys").length(); i++)
                                try {
                                    JSONObject sportObject = jsonArray.getJSONObject(i);
                                    Sport sport = new Sport();
                                    sport.setStrBadge(sportObject.getString("strBadge"));
                                    sport.setStrLeague(sportObject.getString("strLeague"));
                                    sport.setStrDescriptionEN(sportObject.getString("strDescriptionEN"));
                                    sport.setStrWebsite(sportObject.getString("strWebsite"));
                                    sport.setStrFanart1(sportObject.getString("strFanart1"));
                                    sport.setStrFanart2(sportObject.getString("strFanart2"));
                                    sport.setStrFanart3(sportObject.getString("strFanart3"));
                                    sport.setStrFanart4(sportObject.getString("strFanart4"));
                                    sports.add(sport);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        /**/

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), sports);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }

        );
        /*JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                JSON_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++){
                            try {
                                JSONObject sportObject = response.getJSONObject(i);
                                Sport sport = new Sport();
                                sport.setStrBadge(sportObject.getString("strBadge"));
                                sport.setStrLeague(sportObject.getString("strLeague"));
                                sport.setStrDescriptionEN(sportObject.getString("strDescriptionEN"));
                                sport.setStrWebsite(sportObject.getString("strWebsite"));
                                sports.add(sport);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), sports);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }

        );*/
        queue.add(jsonObjectRequest);
    }

    public String createUrl(){
        Intent intent = getIntent();
        String country = intent.getStringExtra(MainActivity.EXTRA_TEXT_NAME);
        String url = "https://www.thesportsdb.com/api/v1/json/2/search_all_leagues.php?c=" + country + "&s=Soccer";
        return url;
    }
}