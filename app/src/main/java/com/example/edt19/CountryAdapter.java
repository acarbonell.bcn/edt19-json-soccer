package com.example.edt19;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CountryAdapter extends ArrayAdapter<Country> {
    public CountryAdapter(@NonNull Context context, ArrayList<Country> countryArrayList) {
        super(context, 0, countryArrayList);
    }

    public View initView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_row, parent, false);
        }

        ImageView imgCountry1 = convertView.findViewById(R.id.imgCounrty);
        TextView textCountry = convertView.findViewById(R.id.textCountry);
        Country item = getItem(position);
        if (convertView != null){
            Picasso.get().load(item.getImgCountry()).fit().centerCrop().into(imgCountry1);
            //imgCountry.setImageResource(R.drawable.canada);
            textCountry.setText(item.getTextCountry());
        }
        return convertView;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView, parent);
    }
}
